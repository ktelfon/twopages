package com.example.twopageapp.repo;

import com.example.twopageapp.model.News;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NewsRepo extends JpaRepository<News, Long> {
}
