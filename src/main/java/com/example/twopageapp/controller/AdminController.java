package com.example.twopageapp.controller;

import com.example.twopageapp.model.News;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class AdminController {

    @GetMapping("/admin")
    public String getIndex(Model model){
        model.addAttribute("newNews", new News());
        return "admin_page";
    }
}
