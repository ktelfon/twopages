package com.example.twopageapp.controller;

import com.example.twopageapp.model.News;
import com.example.twopageapp.repo.NewsRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequiredArgsConstructor
@RequestMapping("/news")
public class NewsController {

    private final NewsRepo newsRepo;

    @GetMapping()
    public String getNews(Model model){
        model.addAttribute("allNews", newsRepo.findAll());
        return "news";
    }

    @PostMapping("/create")
    public String create(News news){
        newsRepo.save(news);
        return "redirect:/admin";
    }
}
