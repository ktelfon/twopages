package com.example.twopageapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TwopageappApplication {

	public static void main(String[] args) {
		SpringApplication.run(TwopageappApplication.class, args);
	}

}
